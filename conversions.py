#!/usr/bin/python3

import curses
import netaddr
import re


class Conversions:
    def __init__(self, screen):
        self.height, self.width = screen.getmaxyx()
        self.input = ""
        self.output = {
            "INPUT": "",
            "HEX": "",
            "DEC": "",
            "IP": "",
            "VENDOR": ""
        }
        self.clear = self.output.copy()
        curses.noecho()
        curses.cbreak()
        self.screen = screen
        self.draw()

    def refresh(self):
        self.draw()
        key = self.screen.getch()
        if key == 10 or key == 13 or key == curses.KEY_ENTER:
            self.convert()
        elif key == 263 or key == curses.KEY_BACKSPACE:
            self.input = self.input[:-1]
        else:
            self.input += chr(key)

    def draw(self):
        self.screen.erase()
        self.screen.addstr(0, 0, "-" * self.width)
        self.screen.addstr(1, 0, "|" + self.output["INPUT"].center(self.width-2) + "|")
        self.screen.addstr(2, 0, "-" * self.width)
        self.screen.addstr(3, 0, "| HEX     | " + self.output["HEX"].ljust(self.width-13) + "|")
        self.screen.addstr(4, 0, "| VENDOR  | " + self.output["VENDOR"].ljust(self.width-13) + "|")
        self.screen.addstr(5, 0, "| IP      | " + self.output["IP"].ljust(self.width-13) + "|")
        self.screen.addstr(6, 0, "| DEC     | " + self.output["DEC"].ljust(self.width-13) + "|")
        self.screen.addstr(7, 0, "-" * self.width)
        self.screen.addstr(10, 1, "Convert: {}".format(self.input))
        self.screen.move(10, 10+len(self.input))
        self.screen.refresh()

    def convert(self):
        self.output = self.clear.copy()
        self.input = self.input.replace(" ", "")
        if self.is_hex() and not self.is_dec():
            self.output["HEX"] = self.input
            self.hex_to_dec()
            if len(self.input) == 8:
                self.hex_to_ip()
        elif self.is_dec():
            self.output["DEC"] = self.input
            self.dec_to_hex()
        elif self.is_ip():
            self.output["IP"] = self.input
            self.ip_to_hex()

        if self.is_mac():
            self.print_manufacturer()

        self.output["INPUT"] = self.input
        self.input = ""

    def is_hex(self):
        return re.match(r'^(0x)?[0-9a-fA-F]*$', self.input)

    def is_dec(self):
        return re.match(r'^[0-9]*$', self.input)

    def is_ip(self):
        return re.match(r'^([0-9]{1,3}\.){3}[0-9]{1,3}$', self.input)

    def is_mac(self):
        return re.match(r'^(([0-9A-Fa-f]{2}[:-]?){6}|([0-9A-Fa-f]{4}\.){2}[0-9A-Fa-f]{4})$', self.input)

    def dec_to_hex(self):
        self.output["HEX"] = hex(int(self.input))

    def hex_to_ip(self):
        self.output["IP"] = ".".join([str(int(self.input[i:i+2], 16)) for i in range(0, 8, 2)])

    def hex_to_dec(self):
        self.output["DEC"] = str(int(self.input, 16))

    def ip_to_hex(self):
        self.output["HEX"] = "".join([('0' + hex(int(octet)).split('x')[1])[-2:] for octet in self.input.split(".")])

    def print_manufacturer(self):
        try:
            self.output["VENDOR"] = netaddr.EUI(self.input).oui.registration().org
        except netaddr.core.NotRegisteredError:
            self.output["VENDOR"] = "UNKNOWN"


def run(stdscr):
    c = Conversions(stdscr)
    while True:
        c.refresh()


if __name__ == '__main__':
    curses.wrapper(run)
