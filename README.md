This is a collection of random tools I've made with Python. Feel free to use them.

## Tools
### Conversions
This is a simple program I made to do quick conversions between hex, dec, ip's, mac's,
etc. It also does lookups on mac address manufacturers with netaddr.

##### Required Packages
- netaddr

##### Note
If mac vendors appear incorrect, delete the `oui.idx` file from netaddr run the ieee.py
script in the same folder.

For example:  
```
rm ~/.local/lib/python3.8/site-packages/netaddr/eui/oui.idx
python3.8 ~/.local/lib/python3.8/site-packages/netaddr/eui/ieee.py
```